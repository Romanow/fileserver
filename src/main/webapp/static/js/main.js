var upload = function () {
    var properties = {};
    var lastMarker = 'last';
    var fileLabelClass = 'file-name-label';
    var fileEditClass = 'file-dialog-button';
    var fileRemoveClass = 'file-remove-button';

    var openFileDialog = function ($button) {
        $button.parent().siblings(':file').click();
        return false;
    };

    var selectFile = function ($file) {
        var selectedFile = $file.val();
        if (selectedFile) {
            var changed = $file.attr('data-changed');
            if (!changed) {
                $file.attr('data-changed', true);

                var template = $("#upload-template").html();
                var $block = Handlebars.compile(template);

                $('.' + lastMarker)
                    .removeClass(lastMarker)
                    .after($block);

                var $parentDiv = $file.parent();
                if (!($parentDiv.find('.' + fileRemoveClass).length)) {
                    var $buttonGroup = $parentDiv.children('.btn-group');
                    var $removeButton =
                        $('<button/>', {
                            'class': 'btn btn-default ' + fileRemoveClass,
                            'title': 'Удалить файл',
                            'type': 'button'
                        }).append('&nbsp;').
                            append($('<i/>', {'class': 'fa fa-remove fa-fw red-text'}));

                    $buttonGroup.append($removeButton);
                }
            }

            var $fileNameLabel = $file.siblings('.' + fileLabelClass);
            var $fileNameInput = $file.siblings(':text');

            var fileName = selectedFile.split(/(\\|\/)/g).pop();
            $fileNameLabel.text(fileName);
            $fileNameInput.val(fileName);
        }
    };

    var removeFile = function($file) {
        $file.parents('.form-group').remove();
    };

    return {
        initializeForm: function(baseUrl) {
            var $form = $('form');
            $form.on('click', '.' + fileEditClass, function () {
                openFileDialog($(this));
            });

            $form.on('click', '.' + fileRemoveClass, function () {
                removeFile($(this));
            });

            $form.on('change', ':file', function () {
                selectFile($(this));
            });

            $.extend(properties, { baseUrl: baseUrl });
        },

        initializeFileUpload: function() {
            var $progress = $('.progress');
            $progress.hide();

            var $last = $('.' + lastMarker);
            var $progressBar = $('.progress-bar');
            var $percent = $('.percent');
            $('form')
                .ajaxForm({
                    beforeSend: function() {
                        $last.hide();
                        $progress.show();

                        var percentVal = '0%';
                        $progressBar.width(percentVal);
                        $percent.text(percentVal);
                    },
                    uploadProgress: function(event, position, total, percentComplete) {
                        var percentVal = percentComplete + '%';
                        $progressBar.width(percentVal);
                        $percent.text(percentVal);
                    },
                    success: function(response) {
                        var percentVal = '100%';
                        $progressBar.width(percentVal);
                        $percent.text(percentVal);
                        $progress.hide();

                        window.location.href = properties.baseUrl + '/uploaded?id=' + response;
                    },
                    error: function(response) {
                        $last.show();
                        $progress.hide();

                        noty(
                            {
                                text: response.responseText,
                                type: 'error',
                                layout: 'topRight',
                                timeout: 5000
                            });
                    }
                });
        }
    }
}();