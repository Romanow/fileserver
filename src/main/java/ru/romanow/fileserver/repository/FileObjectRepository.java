package ru.romanow.fileserver.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import ru.romanow.fileserver.domain.FileObject;

/**
 * Created by ronin on 04.11.15
 */
public interface FileObjectRepository
        extends MongoRepository<FileObject, String>,
                QueryDslPredicateExecutor<FileObject> {}
