package ru.romanow.fileserver.domain;

import java.time.LocalDateTime;

/**
 * Created by ronin on 06.11.15
 */
public interface Auditable {
    LocalDateTime getCreateTime();

    void setCreateTime(LocalDateTime dateTime);

    LocalDateTime getLastRequestTime();

    void setLastRequestTime(LocalDateTime dateTime);
}
