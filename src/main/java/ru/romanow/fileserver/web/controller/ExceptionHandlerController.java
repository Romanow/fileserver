package ru.romanow.fileserver.web.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import ru.romanow.fileserver.web.exception.DocumentNotFoundException;
import ru.romanow.fileserver.web.exception.DownloadException;
import ru.romanow.fileserver.web.exception.FileProcessingException;

/**
 * Created by ronin on 06.11.15
 */
@ControllerAdvice
public class ExceptionHandlerController {

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(FileProcessingException.class)
    public @ResponseBody String exception(Exception exception) {
        return exception.getMessage();
    }

    @ExceptionHandler({ DocumentNotFoundException.class, DownloadException.class })
    public ModelAndView fileNotFound(Exception exception) {
        return new ModelAndView("private/error", "error", exception.getMessage());
    }
}
