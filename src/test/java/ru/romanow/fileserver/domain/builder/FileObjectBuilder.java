package ru.romanow.fileserver.domain.builder;

import ru.romanow.fileserver.domain.FileObject;

import java.time.LocalDateTime;

/**
* Created by ronin on 07.11.15
*/
public class FileObjectBuilder {
    private String id;
    private LocalDateTime createTime;
    private LocalDateTime lastRequestTime;
    private String fileName;
    private String contentType;
    private String fileId;

    private FileObjectBuilder() {}

    public static FileObjectBuilder builder() {
        return new FileObjectBuilder();
    }

    public FileObjectBuilder withId(String id) {
        this.id = id;
        return this;
    }

    public FileObjectBuilder withCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
        return this;
    }

    public FileObjectBuilder withLastRequestTime(LocalDateTime lastRequestTime) {
        this.lastRequestTime = lastRequestTime;
        return this;
    }

    public FileObjectBuilder withFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public FileObjectBuilder withContentType(String contentType) {
        this.contentType = contentType;
        return this;
    }

    public FileObjectBuilder withFileId(String fileId) {
        this.fileId = fileId;
        return this;
    }

    public FileObject build() {
        FileObject fileObject = new FileObject();
        fileObject.setId(id);
        fileObject.setCreateTime(createTime);
        fileObject.setLastRequestTime(lastRequestTime);
        fileObject.setFileName(fileName);
        fileObject.setContentType(contentType);
        fileObject.setFileId(fileId);
        return fileObject;
    }
}
