package ru.romanow.fileserver;

import com.mongodb.Mongo;
import cz.jirutka.spring.embedmongo.EmbeddedMongoBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.stereotype.Repository;

/**
 * Created by ronin on 07.11.15
 */
@Configuration
@ComponentScan(basePackages = "ru.romanow.fileserver.repository",
        includeFilters = @ComponentScan.Filter(Repository.class))
@EnableMongoRepositories(basePackages = "ru.romanow.fileserver.repository")
public class PersistenceTestConfiguration
        extends PersistenceConfiguration {

    @Bean(destroyMethod = "close")
    @Override
    public Mongo mongo() throws Exception {
        return new EmbeddedMongoBuilder()
                .bindIp("127.0.0.1")
                .port(27027)
                .build();
    }
}
