package ru.romanow.fileserver.repository;

import com.mongodb.gridfs.GridFSDBFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Repository;

import java.io.InputStream;

/**
 * Created by ronin on 06.11.15
 */
@Repository
public class FilePersistenceDaoImpl
        implements FilePersistenceDao {

    @Autowired
    private GridFsTemplate gridFsTemplate;

    @Override
    public String persistFile(InputStream stream, String fileName, String contentType) {
        return gridFsTemplate.store(stream, fileName, contentType).getId().toString();
    }

    @Override
    public GridFsResource getFileResource(String fileId) {
        GridFSDBFile file = gridFsTemplate.findOne(Query.query(Criteria.where("_id").is(fileId)));
        if (file != null) {
            return new GridFsResource(file);
        }
        return null;
    }

    @Override
    public void deleteFile(String fileId) {
        gridFsTemplate.delete(Query.query(Criteria.where("_id").is(fileId)));
    }
}
