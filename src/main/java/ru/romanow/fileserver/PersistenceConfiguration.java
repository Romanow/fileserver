package ru.romanow.fileserver;

import com.google.common.collect.Lists;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.convert.CustomConversions;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.stereotype.Repository;
import ru.romanow.fileserver.util.converter.LocalDateTimeToStringConverter;
import ru.romanow.fileserver.util.converter.StringToLocalDateTimeConverter;

/**
 * Created by ronin on 06.11.15
 */
@Configuration
@ComponentScan(basePackages = "ru.romanow.fileserver.repository",
        includeFilters = @ComponentScan.Filter(Repository.class))
@EnableMongoRepositories(basePackages = "ru.romanow.fileserver.repository")
public class PersistenceConfiguration
        extends AbstractMongoConfiguration {

    @Value("${mongodb.port}")
    private Integer port;

    @Value("${mongodb.host}")
    private String host;

    @Value("${mongodb.database}")
    private String database;

    @Override
    protected String getDatabaseName() {
        return database;
    }

    @Bean
    @Override
    public CustomConversions customConversions() {
        return new CustomConversions(
                Lists.newArrayList(new LocalDateTimeToStringConverter(),
                                   new StringToLocalDateTimeConverter()));
    }

    @Bean(destroyMethod = "close")
    @Override
    public Mongo mongo() throws Exception {
        return new MongoClient(host, port);
    }

    @Bean
    @Autowired
    public GridFsTemplate gridFsTemplate(MongoDbFactory mongoDbFactory,
                                         MappingMongoConverter mappingMongoConverter)
            throws Exception {
        return new GridFsTemplate(mongoDbFactory, mappingMongoConverter);
    }
}
