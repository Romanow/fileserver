package ru.romanow.fileserver;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;

/**
 * Created by ronin on 03.11.15
 */
@Configuration
@ComponentScan(basePackages = "ru.romanow.fileserver",
        excludeFilters = { @ComponentScan.Filter(Configuration.class),
                           @ComponentScan.Filter(Repository.class),
                           @ComponentScan.Filter(Controller.class) })
@Import(PersistenceConfiguration.class)
public class ApplicationConfiguration {}
