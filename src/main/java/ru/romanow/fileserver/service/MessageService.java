package ru.romanow.fileserver.service;

/**
 * Created by ronin on 06.11.15
 */
public interface MessageService {
    String getMessage(String code, Object ... args);
}
