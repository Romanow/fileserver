<!doctype html>

<%@ page language="java" pageEncoding="utf8" contentType="text/html;charset=UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<html>
<head>
    <title><spring:message code="main.title"/></title>

    <link rel="shortcut icon" href="<c:url value="/static/favicon.ico"/>" type="image/x-icon">

    <link rel="stylesheet" href="<c:url value="/static/libs/bootstrap-3.3.5/css/bootstrap.min.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/static/libs/font-awesome-4.3.0/css/font-awesome.min.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/static/css/style.css"/>"/>

    <script type="text/javascript" src="<c:url value="/static/libs/jquery-2.1.4/js/jquery-2.1.4.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/static/libs/jquery-form-3.51/js/jquery.form.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/static/libs/handlebars-4.0.5/js/handlebars.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/static/libs/noty-2.3.7/js/jquery.noty.packaged.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/static/js/main.js"/>"></script>
</head>
<body>
    <tiles:insertAttribute name="content"/>
</body>
</html>
