# Настройка окружения

## Настройка mongo
В файле mongod.conf прописать:
```
systemLog:
  destination: file
  path: /usr/local/var/log/mongodb/mongo.log
  logAppend: true
storage:
  dbPath: /Users/ronin/Develop/DB/MongoDB
net:
  bindIp: 127.0.0.1
```
Старт приложения `mongod -f /usr/local/etc/mongod.conf`