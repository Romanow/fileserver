package ru.romanow.fileserver.domain;

import com.google.common.base.MoreObjects;
import com.mysema.query.annotations.QueryEntity;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by ronin on 03.11.15
 */
@QueryEntity
@Document(collection = "files")
public class FileObject
        implements Auditable,
                   Serializable {

    private static final long serialVersionUID = -4451209125076933066L;

    @Id
    private String id;
    private LocalDateTime createTime;
    private LocalDateTime lastRequestTime;
    private String fileName;
    private String contentType;
    private String fileId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    @Override
    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    @Override
    public LocalDateTime getLastRequestTime() {
        return lastRequestTime;
    }

    @Override
    public void setLastRequestTime(LocalDateTime lastRequestTime) {
        this.lastRequestTime = lastRequestTime;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FileObject)) return false;

        FileObject that = (FileObject) o;
        return fileId.equals(that.fileId) &&
                !(fileName != null ?
                        !fileName.equals(that.fileName) :
                        that.fileName != null) && id.equals(that.id);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (fileName != null ? fileName.hashCode() : 0);
        result = 31 * result + fileId.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                          .add("id", id)
                          .add("fileName", fileName)
                          .add("contentType", contentType)
                          .toString();
    }
}
