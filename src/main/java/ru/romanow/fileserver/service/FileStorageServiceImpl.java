package ru.romanow.fileserver.service;

import com.google.common.collect.Lists;
import org.apache.commons.compress.archivers.ArchiveOutputStream;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.compress.utils.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.romanow.fileserver.domain.FileList;
import ru.romanow.fileserver.domain.FileObject;
import ru.romanow.fileserver.domain.QFileObject;
import ru.romanow.fileserver.model.ResourceDescriptor;
import ru.romanow.fileserver.repository.FileListRepository;
import ru.romanow.fileserver.repository.FileObjectRepository;
import ru.romanow.fileserver.repository.FilePersistenceDao;
import ru.romanow.fileserver.web.exception.DocumentNotFoundException;
import ru.romanow.fileserver.web.exception.DownloadException;
import ru.romanow.fileserver.web.exception.FileProcessingException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by ronin on 21.12.14
 */
@Service
public class FileStorageServiceImpl
        implements FileStorageService {
    private static final Logger logger = LoggerFactory.getLogger(FileStorageService.class);

    @Autowired
    private FileObjectRepository fileObjectRepository;

    @Autowired
    private FileListRepository fileListRepository;

    @Autowired
    private FilePersistenceDao filePersistenceDao;

    @Autowired
    private MessageService messageService;

    @Override
    public FileList uploadFiles(List<MultipartFile> files) {
        FileList fileList = new FileList();
        fileList.setCreateTime(LocalDateTime.now());
        fileListRepository.save(fileList);

        files.stream()
             .filter(file -> !file.isEmpty())
             .forEach(file -> {
                 FileObject fileObject = new FileObject();
                 fileObject.setFileName(file.getOriginalFilename());
                 fileObject.setContentType(file.getContentType());
                 fileObject.setCreateTime(LocalDateTime.now());
                 try {
                     String fileId = filePersistenceDao.persistFile(file.getInputStream(),
                                                                    fileObject.getFileName(),
                                                                    file.getContentType());
                     fileObject.setFileId(fileId);
                 } catch (IOException exception) {
                     logger.error("Невозможно открыть загруженный файл [" + fileObject.getFileName() + "]", exception);
                     throw new FileProcessingException(
                             messageService.getMessage("upload.file.exception", fileObject.getFileName()), exception);
                 }
                 fileObjectRepository.save(fileObject);

                 fileList.addFile(fileObject);
                 logger.debug("Файл [{}:{}] сохранен в коллекцию [{}]",
                              fileObject.getFileName(), fileObject.getId(), fileList.getId());
             });

        logger.debug("Сохранены {} файлов в коолекцию [{}]",
                     fileList.getFiles().size(), fileList.getId());

        return fileListRepository.save(fileList);
    }

    @Override
    public @Nullable ResourceDescriptor downloadFile(@Nonnull String id) {
        FileObject fileObject = fileObjectRepository.findOne(id);
        if (fileObject != null) {
            GridFsResource resource = filePersistenceDao.getFileResource(fileObject.getFileId());
            try {
                return new ResourceDescriptor(resource,
                                              resource.getFilename(),
                                              resource.getContentType(),
                                              resource.contentLength());
            } catch (IOException exception) {
                logger.error("Невозможно открыть загруженный из базы файл [" + id + "]:[" +
                                     fileObject.getFileName() + "]", exception);
                throw new DownloadException(
                        messageService.getMessage("download.file.broken", fileObject.getFileName()), exception);
            }
        } else {
            throw new DocumentNotFoundException(messageService.getMessage("download.file.not.found", id));
        }
    }

    @Override
    public ResourceDescriptor downloadFileCollection(@Nonnull String id) {
        FileList collection = fileListRepository.findOne(id);
        if (collection != null) {
            ByteArrayOutputStream stream = null;
            ZipArchiveOutputStream archiveStream = null;
            try {
                stream = new ByteArrayOutputStream();
                archiveStream = new ZipArchiveOutputStream(stream);
                for (FileObject fileObject : collection.getFiles()) {
                    writeFileToArchive(archiveStream, fileObject);
                }
            } finally {
                if (archiveStream != null) {
                    IOUtils.closeQuietly(archiveStream);
                }

                if (stream != null) {
                    IOUtils.closeQuietly(stream);
                }
            }

            return new ResourceDescriptor(
                    new ByteArrayResource(stream.toByteArray()),
                    "archive.zip", "application/zip", stream.size());
        } else {
            throw new DocumentNotFoundException(messageService.getMessage("download.file.not.found", id));
        }
    }

    @Override
    public List<FileObject> getOldFiles(LocalDateTime date) {
        QFileObject fileObject = QFileObject.fileObject;
        return Lists.newArrayList(
                fileObjectRepository.findAll(
                        fileObject.createTime.before(date).
                                and(fileObject.lastRequestTime.isNull().
                                        or(fileObject.lastRequestTime.before(date))
                                )
                )
        );
    }

    @Override
    public void deleteFiles(FileObject fileObject) {
        String fileId = fileObject.getFileId();
        filePersistenceDao.deleteFile(fileId);
        fileObjectRepository.delete(fileObject);
        // удалять еще и коллекцию
    }


    @Override
    public @Nullable FileList getFiles(String id) {
        return fileListRepository.findOne(id);
    }

    private void writeFileToArchive(final ArchiveOutputStream stream, final FileObject fileObject) {
        if (fileObject.getFileId() != null) {
            try {
                GridFsResource resource =
                        filePersistenceDao.getFileResource(fileObject.getFileId());

                ZipArchiveEntry entry =
                        createArchiveEntry(fileObject.getFileName(), resource.contentLength());

                stream.putArchiveEntry(entry);
                IOUtils.copy(resource.getInputStream(), stream);
                stream.closeArchiveEntry();
            } catch (IOException exception) {
                logger.error("Невозможно открыть загруженный из базы файл [" + fileObject.getFileId() + "]:[" +
                                     fileObject.getFileName() + "]", exception);
                throw new DownloadException(
                        messageService.getMessage("download.file.broken", fileObject.getFileName()), exception);
            }
        } else {
            logger.error("Файл [{}:{}] содержит пустую ссылку на файл", fileObject.getId(), fileObject.getFileName());
        }
    }

    private ZipArchiveEntry createArchiveEntry(String fileName, long length) {
        ZipArchiveEntry entry = new ZipArchiveEntry(fileName);
        entry.setSize(length);
        return entry;
    }
}
