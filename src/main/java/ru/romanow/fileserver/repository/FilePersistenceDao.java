package ru.romanow.fileserver.repository;

import org.springframework.data.mongodb.gridfs.GridFsResource;

import java.io.InputStream;

/**
 * Created by ronin on 06.11.15
 */
public interface FilePersistenceDao {
    String persistFile(InputStream stream, String fileName, String contentType);

    GridFsResource getFileResource(String fileId);

    void deleteFile(String fileId);
}
