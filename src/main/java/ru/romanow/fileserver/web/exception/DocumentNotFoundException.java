package ru.romanow.fileserver.web.exception;

/**
 * Created by ronin on 06.11.15
 */
public class DocumentNotFoundException
        extends RuntimeException {
    private static final long serialVersionUID = 240115411502644589L;

    public DocumentNotFoundException(String message) {
        super(message);
    }
}
