package ru.romanow.fileserver.service;

import org.springframework.web.multipart.MultipartFile;
import ru.romanow.fileserver.domain.FileList;
import ru.romanow.fileserver.domain.FileObject;
import ru.romanow.fileserver.model.ResourceDescriptor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by ronin on 03.11.15
 */
public interface FileStorageService {
    FileList uploadFiles(List<MultipartFile> files);

    @Nullable ResourceDescriptor downloadFile(@Nonnull String id);

    @Nullable ResourceDescriptor downloadFileCollection(@Nonnull String id);

    List<FileObject> getOldFiles(LocalDateTime date);

    void deleteFiles(FileObject fileObject);

    @Nullable FileList getFiles(String id);
}
