<%@ page language="java" pageEncoding="utf8" contentType="text/html;charset=UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="col-md-8 col-md-offset-2 top">
    <c:set var="baseURL" value="${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, pageContext.request.contextPath)}" />

    <%--@elvariable id="fileList" type="ru.romanow.fileserver.domain.UploadedEntity"--%>
    <div class="form-horizontal">
        <h1 class="text-center">
            <c:set var="count" value="${fileList.files.size()}"/>
            <c:choose>
                <c:when test="${count == 1}">
                    <spring:message code="uploaded.one.file"/>
                </c:when>
                <c:when test="${count >= 2 && count <= 4}">
                    <spring:message code="uploaded.some.files" arguments="${count}"/>
                </c:when>
                <c:otherwise>
                    <spring:message code="uploaded.many.files" arguments="${count}"/>
                </c:otherwise>
            </c:choose>
        </h1>

        <div class="form-group">
            <label class="control-label multiline-text col-md-5 blue-text" for="collection-download">
                <spring:message code="uploaded.collection.download.label"/>
            </label>

            <div class="input-group col-md-7">
                <input id="collection-download" type="text" class="form-control" value="${baseURL}/download/${fileList.id}" readonly>
                <span class="input-group-btn">
                    <a href="${baseURL}/download/${fileList.id}"
                       class="btn btn-default file-dialog-button"
                       title="<spring:message code="uploaded.collection.download"/>"
                       target="_blank">
                        &nbsp;<i class="fa fa-cloud-download fa-fw blue-text"></i>
                    </a>
                </span>
            </div>
        </div>

        <div class="form-group">&nbsp;</div>

        <h3 class="text-center">
            <spring:message code="uploaded.files"/>
        </h3>

        <c:forEach var="file" items="${fileList.files}">
            <div class="form-group">
                <label class="control-label col-md-5 text-left" for="file-download">${file.fileName}</label>

                <div class="input-group col-md-7">
                    <input id="file-download" type="text" class="form-control" value="${baseURL}/download/file/${file.id}" readonly>
                    <span class="input-group-btn">
                        <a href="${baseURL}/download/file/${file.id}"
                           class="btn btn-default file-dialog-button"
                           title="<spring:message code="uploaded.file.download"/>"
                           target="_blank">
                            &nbsp;<i class="fa fa-cloud-download fa-fw blue-text"></i>
                        </a>
                    </span>
                </div>
            </div>
        </c:forEach>
    </div>
</div>