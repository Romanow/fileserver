package ru.romanow.fileserver;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Created by ronin on 07.11.15
 */
@Configuration
@EnableAutoConfiguration
@Import(ApplicationTestConfiguration.class)
public class StandaloneTestConfiguration {}
