package ru.romanow.fileserver.web.exception;

/**
 * Created by ronin on 06.11.15
 */
public class DownloadException
        extends RuntimeException {
    private static final long serialVersionUID = 6835950662839936250L;

    public DownloadException(String message) {
        super(message);
    }

    public DownloadException(String message, Throwable cause) {
        super(message, cause);
    }
}
