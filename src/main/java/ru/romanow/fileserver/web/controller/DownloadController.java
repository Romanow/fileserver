package ru.romanow.fileserver.web.controller;

import com.google.common.net.HttpHeaders;
import com.google.common.net.UrlEscapers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.romanow.fileserver.model.ResourceDescriptor;
import ru.romanow.fileserver.service.FileStorageService;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by ronin on 04.11.15
 */
@Controller
@RequestMapping("/download")
public class DownloadController {
    private static final Logger logger = LoggerFactory.getLogger(DownloadController.class);

    @Autowired
    private FileStorageService fileStorageService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public @ResponseBody Resource downloadCollection(@PathVariable String id, HttpServletResponse response) {
        ResourceDescriptor descriptor = fileStorageService.downloadFileCollection(id);
        if (descriptor != null) {
            setDownloadHeaders(descriptor, response);
            return descriptor.getResource();
        }
        return null;
    }

    @RequestMapping(value = "/file/{id}", method = RequestMethod.GET)
    public @ResponseBody Resource downloadFile(@PathVariable String id, HttpServletResponse response) {
        ResourceDescriptor descriptor = fileStorageService.downloadFile(id);
        if (descriptor != null) {
            setDownloadHeaders(descriptor, response);
            return descriptor.getResource();
        }
        return null;
    }

    private void setDownloadHeaders(@Nonnull ResourceDescriptor descriptor,
                                    HttpServletResponse response) {
        response.setContentType(descriptor.getContentType());
        response.setContentLengthLong(descriptor.getLength());
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION,
                           "attachment; filename=" +
                                   UrlEscapers.urlFragmentEscaper().escape(descriptor.getName()));
    }
}
