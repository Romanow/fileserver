package ru.romanow.fileserver.domain;

import com.google.common.base.MoreObjects;
import com.mysema.query.annotations.QueryEntity;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ronin on 06.11.15
 */
@QueryEntity
@Document(collection = "collections")
public class FileList
        implements Auditable,
                   Serializable {
    private static final long serialVersionUID = -7696177063038720697L;

    @Id
    protected String id;
    protected LocalDateTime createTime;
    protected LocalDateTime lastRequestTime;
    @DBRef
    private List<FileObject> files;

    public FileList() {
        this.files = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    @Override
    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    @Override
    public LocalDateTime getLastRequestTime() {
        return lastRequestTime;
    }

    @Override
    public void setLastRequestTime(LocalDateTime lastRequestTime) {
        this.lastRequestTime = lastRequestTime;
    }

    public List<FileObject> getFiles() {
        return files;
    }

    public void addFile(FileObject file) {
        this.files.add(file);
    }

    public void setFiles(List<FileObject> files) {
        this.files = files;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FileList)) return false;

        FileList fileList = (FileList) o;
        return id.equals(fileList.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                          .add("id", id)
                          .toString();
    }
}
