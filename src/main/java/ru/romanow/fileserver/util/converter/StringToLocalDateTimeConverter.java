package ru.romanow.fileserver.util.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by ronin on 06.11.15
 */
@ReadingConverter
public class StringToLocalDateTimeConverter
        implements Converter<String, LocalDateTime> {

    @Override
    public LocalDateTime convert(String source) {
        return source != null ? LocalDateTime.parse(source, DateTimeFormatter.ISO_LOCAL_DATE_TIME) : null;
    }
}
