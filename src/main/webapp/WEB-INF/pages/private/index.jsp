<%@ page language="java" pageEncoding="utf8" contentType="text/html;charset=UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="col-md-6 col-md-offset-3 top">
    <h1 class="text-center">
        <spring:message code="upload.files.title"/>
    </h1>

    <c:url var="baseUrl" value="/"/>
    <c:url var="upload" value="/upload"/>
    <form action="${upload}"
          method="post"
          enctype="multipart/form-data"
          accept-charset="utf-8">

        <div class="form-group last">
            <div class="input-group col-md-11">
                <label class="form-control-static file-name-label col-md-offset-1"><spring:message code="upload.file.empty"/></label>
                <input name="file" type="file" class="hidden"/>

                <div class="btn-group pull-right">
                    <button type="button"
                            class="btn btn-default file-dialog-button"
                            title="<spring:message code="upload.file.add"/>">
                        &nbsp;<i class="fa fa-edit fa-fw green-text"></i>
                    </button>
                </div>
            </div>
        </div>

        <div class="input-group col-md-11">
            <div class="progress col-md-offset-1">
                <div class="progress-bar" role="progressbar"
                     aria-valuenow="60"
                     aria-valuemin="0"
                     aria-valuemax="100">
                    <span class="percent">0%</span>
                </div>
            </div>
        </div>

        <button type="submit" class="btn btn-default center-block">
            <spring:message code="upload.submit"/>
        </button>
    </form>
</div>

<script id="upload-template" type="text/x-handlebars-template">
    <div class="form-group last">
        <div class="input-group col-md-11">
            <label class="form-control-static file-name-label col-md-offset-1">
                <spring:message code="upload.file.empty"/>
            </label>
            <input name="file" type="file" class="hidden"/>

            <div class="btn-group pull-right">
                <button type="button"
                        class="btn btn-default file-dialog-button"
                        title="<spring:message code="upload.file.add"/>">
                    &nbsp;<i class="fa fa-edit fa-fw green-text"></i>
                </button>
            </div>
        </div>
    </div>
</script>

<script type="text/javascript">
    $(document).ready(function () {
        upload.initializeForm(${baseUrl});
        upload.initializeFileUpload();
    });
</script>