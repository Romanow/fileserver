package ru.romanow.fileserver.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import ru.romanow.fileserver.domain.FileList;
import ru.romanow.fileserver.service.FileStorageService;

import java.util.List;

/**
 * User: Ronin
 * Date: 13.12.14
 */
@Controller
public class UploadController {

    @Autowired
    private FileStorageService fileStorageService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index() {
        return "private/index";
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public @ResponseBody String upload(@RequestParam("file") List<MultipartFile> files) {
        FileList fileList = fileStorageService.uploadFiles(files);
        return fileList.getId();
    }

    @RequestMapping(value = "/uploaded", method = RequestMethod.GET)
    public ModelAndView successUpload(@RequestParam("id") String id) {
        FileList fileList = fileStorageService.getFiles(id);
        return new ModelAndView("private/success", "fileList", fileList);
    }
}
