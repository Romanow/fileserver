package ru.romanow.fileserver.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.romanow.fileserver.StandaloneTestConfiguration;
import ru.romanow.fileserver.domain.builder.FileObjectBuilder;
import ru.romanow.fileserver.repository.FileObjectRepository;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = StandaloneTestConfiguration.class)
public class FileStorageServiceTest {

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private FileObjectRepository fileObjectRepository;

    @Test
    public void testGetOldFiles() throws Exception {
        prepareTestDataForGetOldFiles();

        assertEquals(4, fileStorageService.getOldFiles(LocalDateTime.now()).size());
        assertEquals(3, fileStorageService.getOldFiles(LocalDateTime.now().minusMinutes(3)).size());
    }

    // region Подготовка тестовых данных
    private void prepareTestDataForGetOldFiles() {
        fileObjectRepository.save(
                FileObjectBuilder.builder()
                                 .withCreateTime(LocalDateTime.now().minusMinutes(10))
                                 .build());

        fileObjectRepository.save(
                FileObjectBuilder.builder()
                                 .withCreateTime(LocalDateTime.now().minusMinutes(5))
                                 .withLastRequestTime(LocalDateTime.now().minusMinutes(4))
                                 .build());

        fileObjectRepository.save(
                FileObjectBuilder.builder()
                                 .withCreateTime(LocalDateTime.now().minusMinutes(10))
                                 .withCreateTime(LocalDateTime.now().minusMinutes(2))
                                 .build());

        fileObjectRepository.save(
                FileObjectBuilder.builder()
                                 .withCreateTime(LocalDateTime.now().minusMinutes(4))
                                 .build());
    }
    // endregion
}