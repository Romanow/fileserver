package ru.romanow.fileserver.model;

import org.springframework.core.io.Resource;

/**
 * Created by ronin on 06.11.15
 */
public class ResourceDescriptor {
    private Resource resource;
    private String name;
    private String contentType;
    private long length;

    public ResourceDescriptor(Resource resource, String name, String contentType, long length) {
        this.resource = resource;
        this.name = name;
        this.contentType = contentType;
        this.length = length;
    }

    public Resource getResource() {
        return resource;
    }

    public String getName() {
        return name;
    }

    public String getContentType() {
        return contentType;
    }

    public long getLength() {
        return length;
    }
}
