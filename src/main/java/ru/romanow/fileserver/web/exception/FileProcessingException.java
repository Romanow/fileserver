package ru.romanow.fileserver.web.exception;

/**
 * Created by ronin on 21.12.14
 */
public class FileProcessingException
        extends RuntimeException {
    private static final long serialVersionUID = 1050891395983874484L;

    public FileProcessingException(String message) {
        super(message);
    }

    public FileProcessingException(String message, Throwable cause) {
        super(message, cause);
    }
}
