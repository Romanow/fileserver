package ru.romanow.fileserver.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.romanow.fileserver.domain.FileObject;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by ronin on 06.11.15
 */
@Service
public class DataCleanerService {

    @Value("${clean.thread.count:1}")
    private Integer threadCount;

    @Autowired
    private FileStorageService fileStorageService;

    @Scheduled(cron = "${cron.schedule}")
    public void cleanOldFiles() throws InterruptedException{
        LocalDateTime date = LocalDateTime.now().minusDays(14);
        List<FileObject> files = fileStorageService.getOldFiles(date);

        CountDownLatch latch = new CountDownLatch(files.size());
        ExecutorService executorService = Executors.newFixedThreadPool(threadCount);
        files.stream()
             .map(file -> new DeleteTask(latch, file))
             .forEach(executorService::execute);

        latch.await();
    }

    private class DeleteTask
            implements Runnable {

        private FileObject file;
        private CountDownLatch latch;

        public DeleteTask(CountDownLatch latch, FileObject file) {
            this.file = file;
            this.latch = latch;
        }

        @Override
        public void run() {
            fileStorageService.deleteFiles(file);
            latch.countDown();
        }
    }
}
