package ru.romanow.fileserver.util.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.WritingConverter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by ronin on 06.11.15
 */
@WritingConverter
public class LocalDateTimeToStringConverter
        implements Converter<LocalDateTime, String> {

    @Override
    public String convert(LocalDateTime source) {
        return source != null ? source.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME) : null;
    }
}
