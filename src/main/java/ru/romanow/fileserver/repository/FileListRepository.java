package ru.romanow.fileserver.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.romanow.fileserver.domain.FileList;

/**
 * Created by ronin on 04.11.15
 */
public interface FileListRepository
        extends MongoRepository<FileList, String> {}
