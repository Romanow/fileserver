package ru.romanow.fileserver.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.romanow.fileserver.StandaloneTestConfiguration;
import ru.romanow.fileserver.domain.builder.FileObjectBuilder;
import ru.romanow.fileserver.repository.FileObjectRepository;

import java.time.LocalDateTime;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = StandaloneTestConfiguration.class)
public class DataCleanerServiceTest {

    private static final int OBJECT_COUNT = 10;

    @Spy
    @Autowired
    private FileStorageServiceImpl fileStorageService;

    @Autowired
    private FileObjectRepository fileObjectRepository;

    @Autowired
    @InjectMocks
    private DataCleanerService dataCleanerService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCleanOldFiles() throws Exception {
        prepareDateCleanOldFields();
        assertEquals(OBJECT_COUNT, fileObjectRepository.findAll().size());

        dataCleanerService.cleanOldFiles();

        verify(fileStorageService, times(OBJECT_COUNT)).deleteFiles(any());
        assertEquals(0, fileObjectRepository.findAll().size());
    }

    // region Подготовка тестовых данных
    private void prepareDateCleanOldFields() {
        IntStream.rangeClosed(1, OBJECT_COUNT)
                 .forEach(i -> {
                     fileObjectRepository.save(
                             FileObjectBuilder.builder()
                                              .withCreateTime(LocalDateTime.now().minusMonths(1))
                                              .build()
                     );
                 });
    }
    // endregion
}